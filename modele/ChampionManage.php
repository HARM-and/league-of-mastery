<?php


class ChampionManage
{

	function getChampNameById($id)
	{
		$ChampsInfo = file_get_contents("http://ddragon.leagueoflegends.com/cdn/12.1.1/data/en_US/champion.json");
  		$ChampsInfo = json_decode($ChampsInfo,true)["data"];

    	foreach ($ChampsInfo as $ChampInfo)
    	{
    		if ($ChampInfo["key"] == $id)
    		{
        		return $ChampInfo["name"];
    		}
    	}
	}

	function cleanChampName($name)
	{
		$name = str_replace("'Z","z",$name);
		$name = str_replace("'S","s",$name);
		$name = str_replace("'G","g",$name);
		$name = str_replace("'K","k",$name);
		$name = str_replace("'","",$name);
		$name = str_replace(" ","",$name);
		return $name;
	}

	function getSplashByName($name)
	{
		return "http://ddragon.leagueoflegends.com/cdn/img/champion/splash/".$name."_0.jpg";
	}
}

