<?php
//Manage
include "modele/ChampionManage.php";
$cm = New ChampionManage();

$config = json_decode(file_get_contents("config.json"),true);
$API_Key = $config[0]["Key"];
$Nb_Champ = $config[0]["Nb_Champ"];


if (isset($_POST["name"]))
{
	$name = $_POST["name"];
}
else
{
	$name = "Sylf0u";
}

$Header = get_headers("https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/$name?api_key=$API_Key", true);

$HeaderCode = substr($Header[0], 9, 3);

if ($HeaderCode == 200)
{
	$response = file_get_contents("https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/$name?api_key=$API_Key");
}
else
{
	$response = file_get_contents("https://euw1.api.riotgames.com/lol/summoner/v4/summoners/by-name/Sylf0u?api_key=$API_Key");
}

$data = json_decode($response, true);
$id = $data["id"];
$name = $data["name"];
$lvl = $data["summonerLevel"];
$iconId = $data["profileIconId"];

$response = file_get_contents("https://euw1.api.riotgames.com/lol/champion-mastery/v4/champion-masteries/by-summoner/$id?api_key=$API_Key");

$data = json_decode($response, true);

if (isset($data[0]))
{
	$BestChamp = $data[0];
	$BestChampId = $BestChamp["championId"];
	$BestChampLvl = $BestChamp["championLevel"];
	$BestChampPts = $BestChamp["championPoints"];
	$BestChampName = $cm->getChampNameById($BestChampId);
	$BestChampBG = "img/bg.jpg";
	//$BestChampNameClean = $cm->cleanChampName($BestChampName);
	//$BestChampBG = $cm->getSplashByName($BestChampNameClean);
}
else
{
	$BestChampLvl = "0";
	$BestChampPts = "0";
	$BestChampName = "None";
	$BestChampBG = "img/bg.jpg";
}

$ChampionListData = json_decode(file_get_contents("https://euw1.api.riotgames.com/lol/champion-mastery/v4/champion-masteries/by-summoner/$id?api_key=$API_Key"),true);

if (isset($ChampionListData[$Nb_Champ-1]))
{
	$ChampionList = [];
	for ($i=0; $i < $Nb_Champ; $i++)
	{ 
		$ChampionList[$i] = $ChampionListData[$i];
	}

	$ChampionMastery = "";
	foreach ($ChampionList as $ChampionData)
	{
		$ChampId = $ChampionData["championId"];
		$ChampLvl = $ChampionData["championLevel"];
		$ChampPts = $ChampionData["championPoints"];
		$ChampName = $cm->getChampNameById($ChampId);
		$ChampNameClean = $cm->cleanChampName($ChampName);

		$CurrentXP = $ChampionData["championPointsSinceLastLevel"];
		$GoalXP = $ChampionData["championPointsUntilNextLevel"];
		$LevelXP = $CurrentXP+$GoalXP;
		$BarreXPTotal = "<div class=\"JaugeXP\" style=\"width: 100%\">.</div>";
		$BarreXPFull = "<div class=\"BarreXPFull\" style=\"width: 100%\">$ChampPts</div>";

        if ($ChampLvl == 7)
        {
        	$ChampionMastery .= "<div class=\"ChampData\"><img class=\"ChampIcon\" src=\"http://ddragon.leagueoflegends.com/cdn/12.1.1/img/champion/".$ChampNameClean.".png\">".$BarreXPTotal.$BarreXPFull."</div>";
        }
        elseif (4 < $ChampLvl && $ChampLvl < 7)
        {
        	$BarreXP = "<div class=\"BarreXP\" style=\"width: ".($CurrentXP/$LevelXP*100)."%\">$ChampPts</div>";
        	$ChampionMastery .= "<div class=\"ChampData\"><img class=\"ChampIcon\" src=\"http://ddragon.leagueoflegends.com/cdn/12.1.1/img/champion/".$ChampNameClean.".png\">".$BarreXPTotal.$BarreXP."</div>";
        }
        else
        {
        	$BarreXP = "<div class=\"BarreXP\" style=\"width: ".($CurrentXP/$LevelXP*100)."%\">$CurrentXP/$LevelXP</div>";
        	$ChampionMastery .= "<div class=\"ChampData\"><img class=\"ChampIcon\" src=\"http://ddragon.leagueoflegends.com/cdn/12.1.1/img/champion/".$ChampNameClean.".png\">".$BarreXPTotal.$BarreXP."</div>";
        }
		
	}
}
else
{
	$ChampionMastery = "";
}




//Mise en page
include_once("vue/vueHead.html");

echo "<body style=\"background-size: cover; background-image: url($BestChampBG)\">
	<div id=\"NavBar\">
		<img class=\"NavButton\" id=\"Logo\" src=\"img/Icon.png\">
		<div class=\"NavButton\" id=\"Burger\">MENU</div>
		<div class=\"NavButton\" id=\"Burger\">MENU</div>
		<div class=\"NavButton\" id=\"Burger\">MENU</div>
		<div class=\"NavButton\" id=\"Burger\">MENU</div>
		<div class=\"NavButton\" id=\"Burger\">MENU</div>
		<div class=\"NavButton\" id=\"Burger\">MENU</div>
		<form method=\"POST\"><input class=\"NavButton\" id=\"SearchBar\" type=\"text\" placeholder=\"Search ...\" name=\"name\"></div></form>
	</div>
	<div id=\"Cadre\">
		<div id=\"NickName\">
			$name
		</div>
		<div id=\"MainData\">
			$ChampionMastery
		</div>
		<div id=\"Summary\">
			<img id=\"PlayerIcon\" src=\"http://ddragon.leagueoflegends.com/cdn/12.1.1/img/profileicon/$iconId.png\">
			<ul>
				LVL : $lvl<br>
				BEST CHAMP :
				<li>Name : $BestChampName</li>
				<li>Level : $BestChampLvl</li>
				<li>Points : $BestChampPts</li>
			</ul>
		</div>
	</div>
</div>";
